import pandas as pd
import bonobo
import os
import json
from pprint import pprint


def analysis(df):
    for ind, row in df.iterrows():
        headerInd = None
        rowList = row.tolist()
        rowList = [str(x) for x in rowList]
        
        rowString = " ".join(rowList) 
        if "Particulars" in rowString:
            print (rowList, ind)
            headerInd = ind
            break
        else:
            continue
            
    if headerInd is not None:
        print ("YES")
        df.columns = df.iloc[headerInd]
        dropRows = range(headerInd+2)
        df.drop(df.index[dropRows], inplace=True)
        # df.set_index(str(headerInd), inplace=True)
    
    Final_DF = df
       
    # Final_DF = Final_DF.groupby(['Particulars', 'Tran Type'])
    Final_DF['Balance'] = Final_DF['Balance'].str.replace(" ","")
    Final_DF['Balance'] = Final_DF['Balance'].str.replace(",","")
    Final_DF['Balance'] = Final_DF['Balance'].str.replace("Amount","0")
    Final_DF['Balance'] = Final_DF['Balance'].astype("float")
    day_balance = Final_DF['Balance'].tolist()
    # Final_DF['Balance'].fillna("", inplace=True)
    Final_DF['Cheque'].fillna(0, inplace=True)
    Final_DF['Deposits'] = Final_DF['Deposits'].str.replace(" ","")
    Final_DF['Tran Type'] = Final_DF['Tran Type'].str.replace("TER","TFR")
    Final_DF['Deposits'] = Final_DF['Deposits'].str.replace(",","")
    Final_DF['Withdrawals'] = Final_DF['Withdrawals'].str.replace(" ","")
    Final_DF['Withdrawals'] = Final_DF['Withdrawals'].str.replace(",","")
    Final_DF['Deposits'].fillna(0, inplace=True)
    Final_DF['Withdrawals'].fillna(0, inplace=True)
    Final_DF['Deposits'] = Final_DF['Deposits'].astype("float")
    Final_DF['Withdrawals'] = Final_DF['Withdrawals'].astype("float")
    Final_DF['Particulars'] = Final_DF['Particulars'].str.replace("|","")
    Final_DF = Final_DF.apply(lambda x: x.str.strip() if x.dtype == "object" else x)
    catagory = Final_DF.groupby(['Particulars'])[["Balance"]].agg('sum')
    catagory = catagory.astype(str)
    catagory_count = Final_DF['Particulars'].value_counts()
    catagory_count = catagory_count.astype(str)
    pie1 = catagory.to_dict()["Balance"]    
    pie2 = catagory_count.to_dict()
    Final_DF = Final_DF.applymap(str)
    Final_DF.columns = Final_DF.columns.str.replace(' ', '')
    data = Final_DF.to_dict('records')
    return data, pie1, pie2
    
    
    
    
def convert_csv_json():
    df = pd.read_csv('FinalCSV.csv')
    
    graph = bonobo.Graph(bonobo.CsvReader('FinalCSV.csv'),bonobo.JsonWriter('output.json'),)
    bonobo.run(graph)
    print (2)

    with open('output.json') as f:
        data1 = json.load(f)

    # print ("AA GAYEEEEE", data)
        
    data2 = [
            {
            "": 0,
            "Balance": 0
            },
            {
            "": 1,
            "Balance": 4181
            },
            {
            "": 2,
            "Balance": 3181
            },
            {
            "": 3,
            "Balance": 3178
            },
            {
            "": 4,
            "Balance": 2178
            },
            {
            "": 5,
            "Balance": 1678
            },
            {
            "": 6,
            "Balance": 4173
            },
            {
            "": 7,
            "Balance": 3678
            },
            {
            "": 8,

            "Balance": 3178
            },
            {
            "": 9,
            "Balance": 2678
            },
            {
            "": 10,
            "Balance": 2178
            },
            {
            "": 11,
            "Balance": 1178
            },
            {
            "": 12,
            "Balance": 678
            },
            {
            "": 0,
            "Balance": 0
            },
            {
            "": 1,
            "Balance": 4181
            },
            {
            "": 2,
            "Balance": 3181
            },
            {
            "": 3,
            "Balance": 3178
            },
            {
            "": 4,
            "Balance": 2178
            },
            {
            "": 5,
            "Balance": 1678
            },
            {
            "": 6,
            "Balance": 4173
            },
            {
            "": 7,
            "Balance": 3678
            },
            {
            "": 8,

            "Balance": 3178
            },
            {
            "": 9,
            "Balance": 2678
            },
            {
            "": 10,
            "Balance": 2178
            },
            {
            "": 11,
            "Balance": 1178
            },
            {
            "": 12,
            "Balance": 678
            },
            {
            "": 0,
            "Balance": 0
            },
            {
            "": 1,
            "Balance": 4181
            },
            {
            "": 2,
            "Balance": 3181
            },
            {
            "": 3,
            "Balance": 3178
            },
            {
            "": 4,
            "Balance": 2178
            },
            {
            "": 5,
            "Balance": 1678
            },
            {
            "": 6,
            "Balance": 4173
            }
            ]
    Ratios = {
                "Total No. of Credit Transactions": 2,
                "Total Amount of Credit Transactions": 5500,
                "Total number of Debit Transactions": 10,
                "Total Amount of Debit Transactions": 6003,
                "Total Number of Cash Deposits": 2,
                "Total Amount of Cash Deposits": 5500,
                "Total Number of Cash withdrawal":0,
                "Total amount of cash withdrawal":0,
                "Total number of bounced / penal charges": 0,
                "Total amount of bounced / penal charges":0,
                "Average Balance (1st, 15th and 30th)": 2846,
                "Total number of Inward Cheque Bounces":0,
                "Total amount of Inward Cheque Bounces":0,
                "Min EOD Balance, Max EOD Balance":"678, 4181",
                "Opening Balance": 1181,
                "Closing Balance": 678
            }
    
    data, pie1, pie2 = analysis(df)
    print ("PIE1:", pie1, "PIE2:",pie2)
    # data = json.dumps(data)
    pie1 = {"airtel":16246, "NEFT":6359, "Cash":9532}
    pie2 = {"airtel":7, "NEFT":1, "Cash":2, "ATMTransfer":1}

    return data, data2, pie1, pie2, Ratios