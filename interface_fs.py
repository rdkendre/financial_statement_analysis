from flask import Flask, request, jsonify, render_template
from flask_dropzone import Dropzone
from flask_cors import CORS
import jwt
from pytz import timezone
import datetime
from functools import wraps
import traceback
import access_db
import os
import pandas as pd
import time
import shutil
import glob
from process_financial_statements import process_financial_statement

app = Flask(__name__)
CORS(app)
basedir = os.path.abspath(os.path.dirname(__file__))

rel_app_url = "http://0.0.0.0:5004"
app_url = {"host": "0.0.0.0", "port": 5004}

app.config.update(
    UPLOADED_PATH=os.path.join(basedir, 'static', 'data', 'input'),
    UPLOADED_PATH_NEW=os.path.join(basedir, 'static', 'data', 'temp'),
    DROPZONE_MAX_FILE_SIZE=30000,
    DROPZONE_MAX_FILES=3000,
    DROPZONE_IN_FORM=True,
    DROPZONE_UPLOAD_ON_CLICK=True,
    DROPZONE_UPLOAD_ACTION='upload_document',  # URL or endpoint
    DROPZONE_UPLOAD_BTN_ID='submit',
)
dropzone = Dropzone(app)
########################################################################################################################
@app.route('/')
def default_screen():
    return render_template('login.html')

@app.route('/login.html')
def login_screen():
    return render_template('login.html')

@app.route('/register.html')
def register_screen():
    return render_template('register.html')

@app.route('/forgot-password.html')
def forgot_screen():
    return render_template('forgot-password.html')

@app.route('/reset-password.html')
def reset_screen():
    return render_template('reset-password.html')

@app.route('/update-profile.html')
def update_profile_screen():
    return render_template('update-profile.html')

@app.route('/validate_screen.html')
def validate_screen():
    return render_template('validate_screen.html')

@app.route('/application-deatils.html')
def application_screen():
    return render_template('application-deatils.html')

@app.route('/ind-insight.html')
def insight_screen():
    return render_template('ind-insight.html')


#######################################################################################################################

def token_required(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        
        data = request.get_json()
        if not data:
            data = {}
            new_data = request.form
            data['token'] = new_data['token']
        if data is None:
            print("USER Request has no body!")
            return jsonify({'message': 'Request has no body!'}), 201

        if 'token' in data:
            token = data['token']
        else:
            print("USER Token is missing!!")
            return jsonify({'message': 'Token is missing!'}), 201

        try:
            response = access_db.get_token(data,'user_registration')
            if response == -2:
                print("USER Token is invalid!!")
                return jsonify({'message': 'Token is invalid'}), 201

            registered_user = response['emailid']
            secret_key = response['secretkey']

            datanew = jwt.decode(token, secret_key)
            current_user = datanew['public_id']

            if not current_user == registered_user:
                return jsonify({'message': 'Not Authorized!'}), 201
        except:
            print(traceback.print_exc())
            
            return jsonify({'message': 'Token is invalid'}), 201

        return f(current_user, *args, **kwargs)
    return decorated

########################################################################################################################

@app.route('/fs/user_register', methods=['POST'])
def register_user():
    data = request.get_json()
    print('User register data', data)
    if data is None:
        return jsonify({'message': 'Request has no body!'}), 201
    registration_type = 'user_registration'
    response = access_db.register_user(data, registration_type)
    print('response',response)
    if response == -1:
        return jsonify({'message': 'User Details Missing!'}), 201
    elif response == -2:
        return jsonify({'message': 'User exists!'}), 201
    elif response == -4:
        return jsonify({'message': 'Mailing error!'}), 201
    return jsonify({'message': 'Registered Successfully!'}), 200

######################################################################################################

@app.route('/fs/activate_customer', methods=['GET'])
def activate_customer():
    data = request.args
    response = access_db.customer_activate(data)

    print('User Testing')

    if response == -2:
        return jsonify({'message': 'Invalid Company'}), 201
    elif response == -1:
        return jsonify({'message': 'Invalid User'}), 201
    elif response == -3:
        return jsonify({'message': 'Company Already Active'}), 201
    elif response == -4:
        return jsonify({'message': 'Admin Already Active'}), 201

    return jsonify({'message': 'Activated Successfully!'}), 200


########################################################################################################################

@app.route('/fs/user_login', methods=['POST'])
def login():
    data = request.get_json()
    print("login checking", data)

    if data is None:
        return jsonify({'message': 'Request has no body!'}), 201

    response,secret_key = access_db.login_user(data)

    print("response,", response, secret_key)
    if response == -1:
        return jsonify({'message': 'Email ID or Password Missing!', 'secretkey': secret_key}), 201
    elif response == -2:
        return jsonify({'message': 'Incorrect Details!', 'secretkey': secret_key}), 201
    elif response == -3:
        return jsonify({'message': 'Company Inactive!', 'secretkey': secret_key}), 201
    token = jwt.encode({'public_id': data['emailid'], 'exp': datetime.datetime.now() +
                        datetime.timedelta(minutes=600)}, secret_key)
    first_name = response
    access_db.update_token(data['emailid'], token.decode('UTF-8'), 'user_registration')
    # print("login output", jsonify({'token': token.decode(
    #     'UTF-8'), 'firstname': first_name, 'registration_type': 'user_registration'}), 200)

    return jsonify({'token': token.decode('UTF-8'), 'firstname': first_name, 'registration_type': 'user_registration'}), 200


########################################################################################################################

@app.route('/fs/updateprofile', methods=['POST'])
@token_required
def update_profile(current_user):
    try:
        emailid = current_user
        response = access_db.profile_update(emailid)
        if not response == -2:
            return jsonify({'result': response}), 200
    except:
        print(traceback.print_exc())
        return jsonify({'message': 'Not successful!'}), 201

    return jsonify({'message': 'User does not exist!'}), 201

########################################################################################################################

@app.route('/fs/updateuser', methods=['POST'])
@token_required
def update_user(current_user):
    data = request.get_json()

    try:
        emailid = current_user
        response = access_db.user_update(emailid, data)
        if not response == -2:
            access_db.logout_user(emailid)
            return jsonify({'message': 'Successful!'}), 200
    except:
        print(traceback.print_exc())
        return jsonify({'message': 'Not successful!'}), 201

    return jsonify({'message': 'User does not exist!'}), 201

########################################################################################################################

@app.route('/fs/forgotpassword', methods=['POST'])
def forgotpassword():
    data = request.get_json()

    if data is None:
        return jsonify({'message': 'Request has no body!'}), 201

    response = access_db.forget_password(data)

    if response == -1:
        return jsonify({'message': 'Email ID Missing!'}), 201
    elif response == -2:
        return jsonify({'message': 'User does not exist!'}), 201

    secret_key = response
    token = jwt.encode({'public_id': data['emailid'], 'exp': datetime.datetime.utcnow() +
                        datetime.timedelta(minutes=5)}, secret_key)
    registration_type = 'NULL'
    access_db.update_token(data['emailid'], token.decode('UTF-8'), registration_type)
    return jsonify({'token': token.decode('UTF-8')}), 200

########################################################################################################################

@app.route('/fs/resetpassword', methods=['POST'])
@token_required
def resetpassword(current_user):
    data = request.get_json()

    if 'password' in data:
        password = data['password']
    else:
        return jsonify({'message': 'Password Missing!'}), 201

    try:
        emailid = current_user
        access_db.reset_password(emailid, password)
        access_db.logout_user(emailid)
        access_db.update_secret(emailid)
    except:
        return jsonify({'message': 'Not successful!'}), 201

    return jsonify({'message': 'Password Updated!'}), 200

########################################################################################################################

@app.route('/fs/logout', methods=['POST'])
@token_required
def logoutuser(current_user):
    try:
        emailid = current_user
        access_db.logout_user(emailid)
    except:
        return jsonify({'message': 'Not successful!'}), 201

    return jsonify({'message': 'Logged out!'}), 200

#########################################################################################################################

@app.route('/fs/user_dashboard', methods=['POST'])
@token_required
def user_dashboard(current_user):
    try:
        print("Cheking User Dashboard  ")
        data = request.get_json()
        response = access_db.user_dashboard_detail(current_user, data)
        
        if not response == -2:
            print("::::::::::::::: Checked User Dashboard")
            return jsonify({'result': response}), 200
    except:
        print(traceback.print_exc())
        return jsonify({'message': 'Not successful!'}), 201
    return jsonify({'result': []}), 200

########################################################################################################################

@app.route('/fs/process_documents', methods=['POST'])
def upload_document():
    try:
        if request.method == 'POST':           
            uploaded_files = request.files.getlist("file")
            folder_path = os.path.join(app.config['UPLOADED_PATH'])
            temp_folder_path = os.path.join(app.config['UPLOADED_PATH_NEW'])
            files = glob.glob(temp_folder_path+'/*')
            for f in files:
                os.remove(f)
            counter = len(os.listdir(folder_path))+1
            folder_path = folder_path+'/Financial_Statement_'+str(counter)
            if not os.path.isdir(folder_path):
                os.makedirs(folder_path)
            for file2 in uploaded_files:
                file_name = file2.filename
                file2.save(os.path.join(folder_path, file_name))
                new_file_name1 = os.path.join(folder_path, file_name)
                new_file_name = new_file_name1.replace("'", "").replace(" ","_")
                os.rename(new_file_name1, new_file_name)
                break
            excel_path,result,gen_info = process_financial_statement(new_file_name,temp_folder_path)
            return jsonify({'message': 'Successful!','Excel_Path' :excel_path}), 200
    except:
        print(traceback.print_exc())
        return jsonify({'message': 'Not successful!'}), 201

#########csv_path2###############################################################################################################
if __name__ == '__main__':
    app.run(host=app_url["host"], port=app_url["port"])
