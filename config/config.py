import os
import pickle
from sklearn.externals import joblib
import numpy as np

# MongoDB variables
EMULYA_AI_MONGODB_URL='localhost'
EMULYA_AI_MONGODB_PORT=27017
EMULYA_AI_MONGODB_DATABASE_NAME='loan_db'
EMULYA_AI_MONGODB_COLLECTION_NAME='Loan'

# Project variables
EMULYA_AI_ROOT_DIR=os.getcwd()
EMULYA_AI_TEMP_DIR='/tmp/emulya-code'

# Google vision variables
EMULYA_AI_GOOGLE_APPLICATION_CREDENTIALS=os.path.join(EMULYA_AI_ROOT_DIR,'config/apikey.json')

# Underwriting variables
EMULYA_AI_UNDERWRITING_FEATURE_CSV=os.path.join(EMULYA_AI_ROOT_DIR,'config/features.csv')
EMULYA_AI_UNDERWRITING_FEATURE_NORMALIZED_CSV=os.path.join(EMULYA_AI_ROOT_DIR,'config/features_normalized.csv')
EMULYA_AI_UNDERWRITING_FEATURE_RBL_CSV=os.path.join(EMULYA_AI_ROOT_DIR,'config/rbl.csv')
EMULYA_AI_UNDERWRITING_FINAL_CSV=os.path.join(EMULYA_AI_ROOT_DIR,'config/final.csv')
EMULYA_AI_DEGREE=os.path.join(EMULYA_AI_ROOT_DIR,'config/world-universities_final.csv')
####
EMULYA_AI_MODEL=os.path.join (EMULYA_AI_ROOT_DIR,'config/finalized_model.sav')
try:
    with open(EMULYA_AI_MODEL, 'rb') as file:
        model = pickle.load(file)
        
    print ("MODEL LOADED !!!")
except Exception as e:
    print ("============ERROR", e)
    model = None

EMULYA_AI_ENSEMBLE = model
####
# EMULYA_AI_GENDERCLASSES_FILE=os.path.join (EMULYA_AI_ROOT_DIR,'config/Genderclasses.npy')
# EMULYA_AI_GENDERCLASSES= np.load(EMULYA_AI_GENDERCLASSES_FILE,encoding='latin1')
# EMULYA_AI_MARITALCLASSES_FILE=os.path.join (EMULYA_AI_ROOT_DIR,'config/Maritalclasses.npy')
# EMULYA_AI_MARITALCLASSES = np.load(EMULYA_AI_MARITALCLASSES_FILE,encoding='latin1')
# EMULYA_AI_QUALIFICATIONCLASSES_FILE=os.path.join (EMULYA_AI_ROOT_DIR,'config/Qualificationclasses.npy')
# EMULYA_AI_QUALIFICATIONCLASSES = np.load(EMULYA_AI_QUALIFICATIONCLASSES_FILE,encoding='latin1')
# EMULYA_AI_EMPLOYMENTCLASSES_FILE=os.path.join (EMULYA_AI_ROOT_DIR,'config/Employmentclasses.npy')
# EMULYA_AI_EMPLOYMENTCLASSES= np.load(EMULYA_AI_EMPLOYMENTCLASSES_FILE,encoding='latin1')
# EMULYA_AI_SOURCEINCOMECLASSES_FILE=os.path.join (EMULYA_AI_ROOT_DIR,'config/SourceIncomeclasses.npy')
# EMULYA_AI_SOURCEINCOMECLASSES = np.load(EMULYA_AI_SOURCEINCOMECLASSES_FILE,encoding='latin1')
# EMULYA_AI_GROSSINCOMECLASSES_FILE=os.path.join (EMULYA_AI_ROOT_DIR,'config/GrossIncomeclasses.npy')
# EMULYA_AI_GROSSINCOMECLASSES = np.load(EMULYA_AI_GROSSINCOMECLASSES_FILE,encoding='latin1')
# EMULYA_AI_MAKECLASSES_FILE=os.path.join (EMULYA_AI_ROOT_DIR,'config/Makeclasses.npy')
# EMULYA_AI_MAKECLASSES = np.load(EMULYA_AI_MAKECLASSES_FILE,encoding='latin1')
# EMULYA_AI_CITY1CLASSES_FILE=os.path.join (EMULYA_AI_ROOT_DIR,'config/City1classes.npy')
# EMULYA_AI_CITY1CLASSES = np.load(EMULYA_AI_CITY1CLASSES_FILE,encoding='latin1')
# EMULYA_AI_STATE1CLASSES_FILE=os.path.join (EMULYA_AI_ROOT_DIR,'config/State1classes.npy')
# EMULYA_AI_STATE1CLASSES = np.load(EMULYA_AI_STATE1CLASSES_FILE,encoding='latin1')
# EMULYA_AI_AT1CLASSES_FILE=os.path.join (EMULYA_AI_ROOT_DIR,'config/AT1classes.npy')
# EMULYA_AI_AT1CLASSES = np.load(EMULYA_AI_AT1CLASSES_FILE,encoding='latin1')
# EMULYA_AI_AT2CLASSES_FILE=os.path.join (EMULYA_AI_ROOT_DIR,'config/AT2classes.npy')
# EMULYA_AI_AT2CLASSES = np.load(EMULYA_AI_AT2CLASSES_FILE,encoding='latin1')
# EMULYA_AI_CITY2CLASSES_FILE=os.path.join (EMULYA_AI_ROOT_DIR,'config/City2classes.npy')
# EMULYA_AI_CITY2CLASSES = np.load(EMULYA_AI_CITY2CLASSES_FILE,encoding='latin1')
# EMULYA_AI_STATE2CLASSES_FILE=os.path.join (EMULYA_AI_ROOT_DIR,'config/State2classes.npy')
# EMULYA_AI_STATE2CLASSES = np.load(EMULYA_AI_STATE2CLASSES_FILE,encoding='latin1')
# EMULYA_AI_PINCODE1CLASSES_FILE=os.path.join (EMULYA_AI_ROOT_DIR,'config/PinCode1classes.npy')
# EMULYA_AI_PINCODE1CLASSES = np.load(EMULYA_AI_PINCODE1CLASSES_FILE,encoding='latin1')
# EMULYA_AI_PINCODE2CLASSES_FILE=os.path.join (EMULYA_AI_ROOT_DIR,'config/PinCode2classes.npy')
# EMULYA_AI_PINCODE2CLASSES = np.load(EMULYA_AI_PINCODE2CLASSES_FILE,encoding='latin1')
# EMULYA_AI_DEALERCLASSES_FILE=os.path.join (EMULYA_AI_ROOT_DIR,'config/Dealerclasses.npy')
# EMULYA_AI_DEALERCLASSES = np.load(EMULYA_AI_DEALERCLASSES_FILE,encoding='latin1')
# EMULYA_AI_SOURCEBRANCHCLASSES_FILE=os.path.join (EMULYA_AI_ROOT_DIR,'config/SourceBranchclasses.npy')
# EMULYA_AI_SOURCEBRANCHCLASSES = np.load(EMULYA_AI_SOURCEBRANCHCLASSES_FILE,encoding='latin1')
####
EMULYA_AI_SCALER_FILE=os.path.join (EMULYA_AI_ROOT_DIR,'config/scaler.sav')
EMULYA_AI_SCALER = pickle.load(open(EMULYA_AI_SCALER_FILE, 'rb'),encoding='latin1')


# Upload dir variables
EMULYA_AI_UPLOAD_DIR_FLASK_REL='static/uploads'
EMULYA_AI_UPLOAD_DIR_ABS=os.path.join(EMULYA_AI_ROOT_DIR,'webserverflask',EMULYA_AI_UPLOAD_DIR_FLASK_REL)

#Template Folder
EMULYA_AI_TEMPLATES=os.path.join(EMULYA_AI_ROOT_DIR,'webserverflask/templates/')

# Contract document dir variables
EMULYA_AI_CONTRACT_PDF_DIR_ABS=os.path.join(EMULYA_AI_UPLOAD_DIR_ABS, 'contract_files/pdfs')
EMULYA_AI_CONTRACT_IMAGE_DIR_ABS=os.path.join(EMULYA_AI_UPLOAD_DIR_ABS, 'contract_files/images')
EMULYA_AI_CONTRACT_IMAGE_DIR_REL=os.path.join(EMULYA_AI_UPLOAD_DIR_FLASK_REL, 'contract_files/images')

# Income document dir variables
# EMULYA_AI_INCOME_IMAGE_DIR_ABS=os.path.join(EMULYA_AI_UPLOAD_DIR_ABS, 'income_files')
# EMULYA_AI_INCOME_IMAGE_DIR_REL=os.path.join(EMULYA_AI_UPLOAD_DIR_FLASK_REL, 'income_files')
EMULYA_AI_INCOME_TEMP_DIR=os.path.join(EMULYA_AI_TEMP_DIR,'income_analysis')
EMULYA_AI_INCOME_IMAGE_DIR_REL = EMULYA_AI_UPLOAD_DIR_FLASK_REL
EMULYA_AI_INCOME_IMAGE_DIR_ABS = EMULYA_AI_UPLOAD_DIR_ABS
