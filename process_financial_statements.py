import os
from pdf2image import convert_from_path
import PyPDF2
import pandas as pd
import boto3
import json
from trp import Document
from page_extraction import get_required_pdf
from pathlib import Path
from notes_detail import notes_detail
from extract_general_information import notes_gen_info
import time


def pdf_image_convert(pdf_file_path,doc_type_list,temp_folder_path):
    pdf_file_name = pdf_file_path.split('/')[-1].split('.')[0]
    pages = convert_from_path(pdf_file_path)
    img_file_path_list = []
    doc_name_list = []
    for i,page in enumerate(pages):
        img_file_path =  temp_folder_path+'/'+pdf_file_name+'_' + doc_type_list[i]+'.jpg'
        print("+++ img_file_path +++",img_file_path)
        if img_file_path_list:
            if img_file_path_list[-1] != img_file_path :
                img_file_path_list.append(img_file_path)
                doc_name_list.append(doc_type_list[i])
        else:
            img_file_path_list.append(img_file_path)
            doc_name_list.append(doc_type_list[i])
        with open(img_file_path,'w') as fil:
            page.save(fil)
    return img_file_path_list,doc_name_list

def process_fs(imgfile,doc_name,excel_path):
    textract = boto3.client('textract')
    with open(imgfile, 'rb') as document2:
        bytes_test2 = bytearray(document2.read())
        response2 = textract.analyze_document(Document={'Bytes': bytes_test2}, FeatureTypes=['TABLES'])
        doc=Document(response2)
    words=[]
    data = []
    for page in doc.pages:
        # print('+++++++++++++++++++++++++++\n',page)
        for line in page.lines:
            words.extend(line.text.split(" "))
        for t_id,table in enumerate(page.tables):
            for r,row in enumerate(table.rows):
                rows= []
                for c,cell in enumerate(row.cells):
                    cell_text = str(cell.text)
                    if cell_text:
                        cell_text = str(cell_text).replace('(','')
                        cell_text = str(cell_text).replace(')','')
                        cell_text = str(cell_text).strip()
                    rows.append(cell_text)
                data.append(rows)
                
        df = pd.DataFrame(data)     
        df = df.dropna(how='all', axis=1) 
        df = df.dropna(how='all', axis=0)
    return df

def get_col_widths(dataframe):
    idx_max = max([len(str(s)) for s in dataframe.index.values] + [len(str(dataframe.index.name))])
    return [idx_max] + [max([len(str(s)) for s in dataframe[col].values] + [len(str(col))]) for col in dataframe.columns]

def process_financial_statement(file_name,temp_folder_path):
    doc_type_list,subset_file_name,notes_file_name = get_required_pdf(file_name,temp_folder_path)
    img_file_path_list,doc_name_list = pdf_image_convert(subset_file_name,doc_type_list,temp_folder_path)
    print('doc_name_list',doc_name_list)

    excel_path = file_name.split('.')[0] + '.xlsx'
    df_list = []
    for i,image_path in enumerate(img_file_path_list):
        print("++++++++ Image File Name +++++++++",image_path)
        df = process_fs(image_path,doc_name_list[i],excel_path)
        df = df.set_index(0)
        df_list.append(df)
    # print(df_list)
    writer = pd.ExcelWriter(excel_path,engine='xlsxwriter')
    for index,sheetname in enumerate(doc_name_list):
        df_list[index].to_excel(writer, sheet_name=sheetname, header = False,index = True)
        worksheet = writer.sheets[sheetname]
        for i, width in enumerate(get_col_widths(df_list[index])):
            worksheet.set_column(i, i, width)

    # start_time = time.time()
    # gen_info = notes_gen_info(notes_file_name)
    # print("+++++ Time for Gen Info ++++++",time.time()-start_time)
    # print("General/ Information of Company ::::::::::",gen_info)

    # df_gen = pd.DataFrame.from_dict(gen_info, orient='index')
    # result = notes_detail(notes_file_name,excel_path)
    # print("Extracted Result from Notes",result)
    result = {}
    gen_info = {}
    # df_gen.to_excel(writer, sheet_name='Company Information', header = False)
    # worksheet = writer.sheets['Company Information']  
    # for i, width in enumerate(get_col_widths(df_gen)):
    #     worksheet.set_column(i, i, width)
    writer.save()
    return excel_path,result,gen_info


if __name__ == '__main__':
    file_name = '/home/rahul/Desktop/Dataset/Financial_Statements/Indian_Annual_Reports/Annual_Report/RSIL_AR_2018_19.pdf'
    temp_folder_path = '/home/rahul/workspace/financial_statement_analysis/static/data/temp/'
    process_financial_statement(file_name,temp_folder_path)