import pandas as pd
import numpy as np
import os
import re
import pdftotext
from PyPDF2 import PdfFileReader, PdfFileWriter


def get_fs_type(text):
    
    text = text.split('\n')
    lines_count = len(text)/5
    text = text[:int(lines_count)]
    text = ' '.join(text)
    text = re.sub(' +', ' ', text)
    print(text)
    header_key_word = {
        'Cash Flow Statement':['consolidated cash flows statements','statement of cash flows','cash flow statement'],
        'Balance Sheet':['balance sheet','balance sheets','statements of financial position','financial position'],
        'Profit and Loss Statement':['statements of income','consolidated profit and loss account','income statement','statement of profit and loss']        
        }
    
    for fs_name,fs_keyword in header_key_word.items():
        for item in fs_keyword:
            if item in text:
                if 'notes to the financial statements' in text or 'financial review' in text or 'notes to consolidated financial statements' in text or 'reconciliation of equity' in text :
                    return ''
                
                document_type = fs_name
                if document_type:
                    if 'consolidated' in text:
                        return 'Consolidated '+ document_type
                return document_type
    return ''

def get_doc_type(text):
    text = text.lower()
    # print(text)
    doc_name = get_fs_type(text)
    doc_name_new = doc_name.replace('Consolidated ','')
    print(":::::::::::::",doc_name)
    if not doc_name:
        return ''
    
    keywords= {
    'Balance Sheet': ['non-current assets','current assets','inventories','liabilities','current liabilities','non-current liabilities',
                                'total liabilities','net assets','non-controlling interest','total equity'],

    'Profit and Loss Statement':['finance costs','selling and distribution expenses','revenue','expenses','cost of sales','gross profit','other income','net income',
                    'loss before income tax','non-controlling interest','basic','diluted','loss for the financial year','return for the year','total comprehensive loss attributable','profit before income tax',
                    'tax expense','basic and diluted','income for the financial year'],
    'Profit and Loss Statement1':['basic and diluted','basic','diluted','revenue','gross profit'],
    'Cash Flow Statement':['net income','cash flow from operating activities']
    }

    for x,y in keywords.items():  
        x = x.replace('1','')
        if x == doc_name_new:
            item_count = []
            for item in y:
                if item in text:
                    if 'reconciliation of equity' in text:
                        return ''
                    item_count.append(item)
                    items_count = len(set(item_count))
                    if items_count/len(y) > 0.3 and doc_name_new == str(x):
                        document_type = str(x)
                        if 'Consolidated' in doc_name:
                            return str(doc_name).replace(' Statement','')
                        return document_type
    return ''
def get_fs_pages(file_name) :
    with open(file_name, "rb") as f:
        pdf = pdftotext.PDF(f)
    count = 0
    fs_page_number = []
    notes_page_number = []
    doc_type_list = []
    # Iterate over all the pages
    for text in pdf:
        print("++++++++++++++ Page Number +++++++++++++++++ ",count)
        count += 1
        doc_type = get_doc_type(text)
        if doc_type:
            fs_page_number.append(count-1)
            doc_type_list.append(doc_type)

        text=text.lower()
        text = text.split('\n')
        lines_count = len(text)/5
        text = text[:int(lines_count)]
        text = ' '.join(text)
        matches = re.search(rf'n\s*o\s*t\s*e\s*s\s*t\s*o\s*t\s*h\s*e\s*f\s*i\s*n\s*a\s*n\s*c\s*i\s*a\s*l\s*s\s*t\s*a\s*t\s*e\s*m\s*e\s*n\s*t\s*s\s*', text,re.IGNORECASE)
        if matches:
            notes_page_number.append(count-1)

    print("FS Page Numbers ",fs_page_number)
    print('Notes page number',notes_page_number)
    return fs_page_number,doc_type_list,notes_page_number

def get_required_pdf(pdf_file_path,temp_folder_path):
    fs_page_number,doc_type_list,notes_page_number = get_fs_pages(pdf_file_path)
    file_base_name = temp_folder_path + '/'+pdf_file_path.split('/')[-1].split('.')[0]
    pdf = PdfFileReader(pdf_file_path)
    pdfWriter = PdfFileWriter()
    notes_page_number = [0]
    for page_num in fs_page_number:
        print("************",page_num)
        pdfWriter.addPage(pdf.getPage(page_num))
    fs_file_name = file_base_name+'_fs.pdf'
    with open('{0}_fs.pdf'.format(file_base_name), 'wb') as fs:
        pdfWriter.write(fs)
        fs.close()

    pdfWriter_notes = PdfFileWriter()
    for page_num in notes_page_number:
        pdfWriter_notes.addPage(pdf.getPage(page_num))

    notes_file_name = file_base_name+'_notes.pdf'
    with open('{0}_notes.pdf'.format(file_base_name), 'wb') as f:
        pdfWriter_notes.write(f)
        f.close()
    return doc_type_list,fs_file_name,notes_file_name


if __name__ == "__main__":
    
    file_name = '/home/rahul/Desktop/Dataset/Financial_Statements/Singapore_Annual_report/Annual_Report/AnnAik Limited.pdf'
    # get_required_pdf(file_name)

