import os
import pandas as pd
import json
import pdftotext
import re


def notes_detail(notes_file,excel_file):
    # notes_file="/home/rahul/Desktop/Dataset/Financial_Statements/Singapore_Annual_report/Annual_Report/AMOS Group Limited_subset1.pdf"
    # excel_file="/home/rahul/Desktop/Dataset/Financial_Statements/Singapore_Annual_report/Annual_Report/SGA/AMOS Group Limited.xlsx"
    doc_name_list=['Profit and Loss Statement', 'Balance Sheet', 'Statements of change in equity']
    with open(notes_file, "rb") as f:
        pdf = pdftotext.PDF(f)
    # print(pdf)

    pattern = re.compile(r'^[-+]?[-0-9]\d*\.\d*|[-+]?\.?[0-9]\d*$')
    all_results={}
    try:
        for doc_type in doc_name_list:
            fin_file=pd.read_excel(excel_file,sheet_name=doc_type)
            # print(fin_file.columns)
            # print(fin_file)
            
            not_null_notes = pd.notnull(fin_file[fin_file.columns[1]]) 
            # print(fin_file[not_null_notes])
            df=fin_file[not_null_notes]
            items_to_search=df[[df.columns[0],df.columns[1]]]
            print(items_to_search)
            full_result={}
            for i,j in items_to_search.iterrows():
                index=items_to_search.loc[i,items_to_search.columns[1]]
                item=items_to_search.loc[i,items_to_search.columns[0]]
                # print(index,item)
                result = pattern.match(str(index).strip())
                if result:
                    index=int(index)
                    item=item.lower()
                    print('index,item',index,item)
                    page_number = []
                    match=""
                    count=0
                    for text in pdf:
                        text=text.lower()
                        count += 1
                        # matches = re.search(rf'{index} \s* {item}', text)
                        # print(text)
                        matches = re.search(rf'{index} \s* {item}', text)
                        if matches:
                            page_number.append(count-1)
                            match=matches.group(0)
                            # print(page_number)
                    full_result[match]=page_number
                    # print("Page Numbers are ",page_number)
            # print('full_result',full_result)
            all_results[doc_type]=full_result
        print('all_results',all_results)
        return all_results
    except:
        pass
    return " "