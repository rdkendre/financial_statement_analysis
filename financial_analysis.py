import os
import config.config as project_configs
from src.general import profit_loss

def results(request):
    file = request.files['image']
    fnaMe = file.filename
    file_name = fnaMe.split('.')[0]

    if fnaMe.split('.')[-1] == 'pdf':
        # pdf_file = os.path.join(project_configs.EMULYA_AI_UPLOAD_DIR_ABS, file_name + '.pdf')
        pdf_file = os.path.join('webserverflask/static/uploads/', file_name + '.pdf')
        print("**********",pdf_file)
        file.save(pdf_file)
        pnl_dict, bs_data = profit_loss.get_balance_sheet_attributes(pdf_file)
        print (pnl_dict["Profit for the year"],pnl_dict["Sales"] )

        Ratio = {}
        Ratio['Profitability_ratio'] = float("{0:.2f}".format(float(pnl_dict["Profit for the year"].replace(",",""))/float(pnl_dict["Sales"].replace(",",""))*100))
        Ratio['Liquidity_Ratio_Current_ratio'] = float("{0:.2f}".format(float(bs_data['total current liabilities'].replace(",",""))/float(bs_data['Total current assets'].replace(",",""))*100))
        Ratio['Debt_equity_ratio'] = float("{0:.2f}".format(float(bs_data['total current liabilities'].replace(",",""))/float(bs_data['Share Capital'].replace(",",""))))
        print("********",Ratio)
        return pnl_dict,bs_data,Ratio

