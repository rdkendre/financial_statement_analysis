import os
import PyPDF2
import pandas as pd
import json
import pdftotext
import re
import en_core_web_sm
nlp = en_core_web_sm.load()
with open('./config/config-business-type.json') as jsFile:
    types = json.load(jsFile)

# if __name__ == "__main__":
def notes_gen_info(notes_file):
    with open(notes_file, "rb") as f:
        pdf = pdftotext.PDF(f)
    page_number=[]
    count=0
    result={'Domicile':'NA','Address':'NA','Business Type(L3)':'NA','Business Type(L2)':'NA'}
    for text in pdf:
        if count>5:
            break
        print("**********Notes Page Number ********",count)
        text=text.replace("\n"," ")
        text=text.replace("\s+"," ")
        count += 1
        matches1 = re.search(r'\s* [general|overview|domicile and activites|corporate information|description of Business] (.*?) 2[.\s] \s* summary of significant accounting policies', text, re.IGNORECASE)
        matches2 = re.search(r' \s* [general|overview|domicile and activites|corporate information|description of Business] (.*?) 2[.\s] \s* basis of preparation', text, re.IGNORECASE)
        
        if matches1 or matches2:
            matches = matches1 if matches1 else matches2
            page_number.append(count-1)
            match=matches.group(1)
            doc=nlp(match)
            for ent in doc.ents:
                if ent.label_=='GPE' and result['Domicile']=='NA':
                    result['Domicile']=' '.join(ent.text.split())
            matches = re.finditer('(\S+\s+){5}(?=[0-9]{6})', match,re.IGNORECASE)
            for m in matches:
                result['Address']=' '.join(m.group().split())
            for keys,item_list in types.items():
                for item in item_list:
                    if item in match and result['Business Type(L3)']=='NA':
                        result['Business Type(L3)']=item
                        result['Business Type(L2)']=keys
            break
    return result