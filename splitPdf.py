import copy
import math
import sys
from PyPDF2 import PdfFileWriter
from PyPDF2 import PdfFileReader

input = PdfFileReader(open("/home/rahul/Downloads/singapore_top30/Singapore Airlines.pdf","rb"))
output = PdfFileWriter()
for i in range(input.getNumPages()):
    p = input.getPage(i)
    q = copy.copy(p)
    q.mediaBox = copy.copy(p.mediaBox)

    o,c = p.mediaBox.lowerLeft
    w, h = p.mediaBox.upperRight

    o,c = math.floor(o), math.floor(c)
    w, h = math.floor(w), math.floor(h)
    m, m1 = math.floor(w/2), math.floor(h/2)

    if w > h:
        # horizontal
        p.mediaBox.upperRight = (m, h)
        p.mediaBox.lowerLeft = (o, c)

        q.mediaBox.upperRight = (w, h)
        q.mediaBox.lowerLeft = (m, c)
    else:
        # vertical
        p.mediaBox.upperRight = (w, h)
        p.mediaBox.lowerLeft = (o, m1)

        q.mediaBox.upperRight = (w, m1)
        q.mediaBox.lowerLeft = (o, c)

    output.addPage(p)
    output.addPage(q)

with open("/home/rahul/Downloads/singapore_top30/Singapore Airlines2.pdf", "wb") as outputStream:
        output.write(outputStream)
