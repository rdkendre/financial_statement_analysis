

import os
from pdf2image import convert_from_path
import PyPDF2
import pandas as pd
import boto3
import json
from trp import Document

file="/home/rahul/Desktop/Dataset/Financial_Statements/Singapore_Annual_report/Annual_Report/Annual_reports/Singapore Airlines.pdf"
pdffile= open(file,'rb')
print(file)
reader = PyPDF2.PdfFileReader(pdffile)
# print(reader.documentInfo)
# print(reader.numPages)
def pdf_image_convert(pdf_file_path):
    
    pdf_file_name = pdf_file_path.split('/')[-1].split('.')[0]
    
    pages = convert_from_path(pdf_file_path)
    for i,page in enumerate(pages):
        img_file_path =  '/'.join(pdf_file_path.split('/')[:-1])+'/'+pdf_file_name+'_'+str(i+1)+'.jpg'
        #img_path = img_file_path+'_'+str(i)
        with open(img_file_path,'w') as fil:
            page.save(fil)



pageObj=reader.getPage(101)
# print(pageObj.extractText()) 


writer = PyPDF2.PdfFileWriter()
pageObj=reader.getPage(101)
writer.addPage(pageObj)
# for page in range(63,94):
#     writer.addPage(reader.getPage(page))
output_filename = '/home/rahul/Desktop/Dataset/Financial_Statements/Singapore_Annual_report/Annual_Report/AMOS/2page.pdf'
with open(output_filename, 'wb') as output:
    writer.write(output)

pdf_image_convert(output_filename)
# # Multi Column

textract = boto3.client('textract')

imgfile='/home/rahul/Desktop/Dataset/Financial_Statements/Singapore_Annual_report/Annual_Report/AMOS/2page_1.jpg'



with open(imgfile, 'rb') as document:
    bytes_test = bytearray(document.read())
    response = textract.analyze_document(Document={'Bytes': bytes_test}, FeatureTypes=['TABLES'])
#doc=Document(response)



columns = []
lines = []
for item in response["Blocks"]:
      if item["BlockType"] == "LINE":
        column_found=False
        for index, column in enumerate(columns):
            bbox_left = item["Geometry"]["BoundingBox"]["Left"]
            bbox_right = item["Geometry"]["BoundingBox"]["Left"] + item["Geometry"]["BoundingBox"]["Width"]
            bbox_centre = item["Geometry"]["BoundingBox"]["Left"] + item["Geometry"]["BoundingBox"]["Width"]/2
            column_centre = column['left'] + column['right']/2

            if (bbox_centre > column['left'] and bbox_centre < column['right']) or (column_centre > bbox_left and column_centre < bbox_right):
                #Bbox appears inside the column
                lines.append([index, item["Text"]])
                column_found=True
                break
        if not column_found:
            columns.append({'left':item["Geometry"]["BoundingBox"]["Left"], 'right':item["Geometry"]["BoundingBox"]["Left"] + item["Geometry"]["BoundingBox"]["Width"]})
            lines.append([len(columns)-1, item["Text"]])

lines.sort(key=lambda x: x[0])
for line in lines:
    print (line[1])


# # Tabular Detection


imgfile2='/home/rahul/Desktop/Dataset/Financial_Statements/Singapore_Annual_report/Annual_Report/AMOS/2page_1.jpg'
with open(imgfile2, 'rb') as document2:
    bytes_test2 = bytearray(document2.read())
    response2 = textract.analyze_document(Document={'Bytes': bytes_test2}, FeatureTypes=['TABLES'])
    doc=Document(response2)

words=[]
col0 = []
col1 = []
col2 = []
col3 = []
col4 = []
for page in doc.pages:
    # print('+++++++++++++++++++++++++++\n',page)
    for line in page.lines:
        #print(line.text.split())
        words.extend(line.text.split(" "))
        #print(tables)
    for t_id,table in enumerate(page.tables):
        for r,row in enumerate(table.rows):
            item=""
            for c,cell in enumerate(row.cells):
                # print(r,c,cell.text)
                if c == 0:
                    col0.append(cell.text)
                if c == 1:
                    col1.append(cell.text)
                if c == 2:
                    col2.append(cell.text)
                if c == 3:
                    col3.append(cell.text)
                if c == 4:
                    col4.append(cell.text)
if col4:
    df = pd.DataFrame(list(zip(col0, col1,col2,col3,col4))) 
else:
    df = pd.DataFrame(list(zip(col0, col1,col2,col3))) 
print(df)
df.to_excel('financial statement.xlsx',index=False,header=False)
