import os
from pdf2image import convert_from_path
import PyPDF2
import pandas as pd
import boto3
import json
from trp import Document
from notes_pdf_extraction import get_required_pdf
from pathlib import Path
import pdftotext
import re

if __name__ == "__main__":
    notes_file="/home/anuja/backup/Dropbox/python_files/files/test/Singapore Airlines_subset1.pdf"
    excel_file="/home/anuja/backup/Dropbox/python_files/files/test/Singapore Airlines.xlsx"
    doc_name_list=['Profit and Loss Statement', 'Balance Sheet', 'Statements of change in equity']
    with open(notes_file, "rb") as f:
        pdf = pdftotext.PDF(f)
    print(pdf)
    pattern = re.compile(r'^[-+]?[-0-9]\d*\.\d*|[-+]?\.?[0-9]\d*$')
    all_results={}
    for doc_type in doc_name_list:
        print(doc_type)
        fin_file=pd.read_excel(excel_file,sheet_name=doc_type)
        # print(fin_file.columns)
        # print(fin_file)
        not_null_notes = pd.notnull(fin_file[fin_file.columns[1]]) 
        # print(fin_file[not_null_notes])
        df=fin_file[not_null_notes]
        items_to_search=df[[df.columns[0],df.columns[1]]]
        # print(items_to_search)
        full_result={}
        for i,j in items_to_search.iterrows():
            index=items_to_search.loc[i,items_to_search.columns[1]]
            item=items_to_search.loc[i,items_to_search.columns[0]]
            # print(index,item)
            result = pattern.match(str(index).strip())
            if result:
                index=int(index)
                item=item.lower()
                # print(index,item)
                page_number = []
                match=""
                count=0
                for text in pdf:
                    text=text.lower()
                    count += 1
                    matches = re.search(rf'{index} \s* {item}', text)
                    if matches:
                        page_number.append(count-1)
                        match=matches.group(0)
                        # print(page_number)
                full_result[match]=page_number
                # print("Page Numbers are ",page_number)
        # print(full_result)
        all_results[doc_type]=full_result
    print(all_results)